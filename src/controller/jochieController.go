package controller

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	informerCoreV1 "k8s.io/client-go/informers/core/v1"
	"k8s.io/client-go/kubernetes"
	corev1 "k8s.io/client-go/kubernetes/typed/core/v1"
	listercorev1 "k8s.io/client-go/listers/core/v1"
	"k8s.io/client-go/tools/cache"
	"log"
	"time"
)

type JochieController struct {
	client          *kubernetes.Clientset
	podGetter       corev1.PodsGetter
	podLister       listercorev1.PodLister
	podListerSynced cache.InformerSynced
}

func NewJochieController(client *kubernetes.Clientset, podInformer informerCoreV1.PodInformer) *JochieController {
	controller := &JochieController{
		client:          client,
		podGetter:       client.CoreV1(),
		podLister:       podInformer.Lister(),
		podListerSynced: podInformer.Informer().HasSynced,
	}

	podInformer.Informer().AddEventHandler(
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				controller.onAdd(obj)
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				controller.onUpdate(oldObj, newObj)
			},
			DeleteFunc: nil,
		},
	)

	return controller
}

func (controller *JochieController) Run(stop <-chan struct{}) {
	log.Print("waiting for cache sync")
	if !cache.WaitForCacheSync(stop, controller.podListerSynced) {
		log.Print("timed out waiting for cache sync")
		return
	}
	go handleExpiredPods(controller.client)
	log.Print("caches are synced")
	log.Print("waiting for stop signal")
	<-stop
	log.Print("received stop signal")
}

func (controller *JochieController) onAdd(obj interface{}) {
	key, err := cache.MetaNamespaceKeyFunc(obj)
	if err != nil {
		log.Print("onAdd: error getting key for %%v:%v", key, err)
	}
	log.Print("onAdd: %v", key)

}

func (controller *JochieController) onUpdate(oldObj, newObj interface{}) {
	oldkey, err := cache.MetaNamespaceKeyFunc(oldObj)
	if err != nil {
		log.Print("onUpdate: error getting key for %v:%v", oldkey, err)
	}
	newkey, err := cache.MetaNamespaceKeyFunc(oldObj)
	if err != nil {
		log.Print("onUpdate: error getting key for %v:%v", newkey, err)
	}
	log.Print("onUpdate: ", oldkey, " => ", newkey)

}

func handleExpiredPods(client *kubernetes.Clientset) {
	for {
		deployments, err := client.AppsV1().Deployments("default").List(metav1.ListOptions{})
		if err != nil {
			log.Print("error listing deployment: ", err)
		}
		for _, deployment := range deployments.Items {
			log.Print("deployment: ", deployment.Annotations)
			if delete, found := deployment.Annotations["deleteOnTimestamp"]; found {
				log.Println(delete)
				log.Println(time.Now().Format(time.RFC3339))
				delTime, err := time.Parse(time.RFC3339, delete)
				if err != nil {
					log.Panic("Error parsing time, ", err)
				}
				if time.Now().Before(delTime) {
					log.Print("keep running")
				} else {
					log.Println("should delete")
					err := client.AppsV1().Deployments("default").Delete(deployment.GetName(), &metav1.DeleteOptions{})
					if err != nil {
						log.Println("error deleting deployment, ", deployment.GetName())
					}
				}
			}
		}
		time.Sleep(10 * time.Second)
	}
}
